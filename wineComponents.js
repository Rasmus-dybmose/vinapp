﻿/// <reference path="libs/angular.min.js" />
(function () {
    "use strict";
    angular.module("wineComponents", [])

    .directive("wineTitle", function () {
        return {
            restrict: "E",
            templateUrl: "wine-title.html"
        };
    })
    .directive("winePanels", function () {
        return {
            restrict: "E",
            templateUrl: "wine-panels.html",

            // ***************************Kode fra codeschool***************************
            controller: function () {
                this.tab = 1;

                this.selectTab = function (setTab) {
                    this.tab = setTab;
                };
                this.isSelected = function (checkTab) {
                    return this.tab === checkTab;
                };
            },
            controllerAs: "panels"
            // ***************************Kode fra codeschool***************************
        }
    })
    .directive("wineDescription", function () {
        return {
            restrict: "E",
            templateUrl: "wine-description.html"
        };
    })
    .directive("wineSpec", function () {
        return {
            restrict: "E",
            templateUrl: "wine-spec.html"
        };
    })
    .directive("wineForm", function () {
        return {
            restrict: "E",
            templateUrl: "wine-form.html",
            controller: function () {
                this.tab = 2;

                this.selectTab = function (setTab) {
                    this.tab = setTab;
                };
                this.isSelected = function (checkTab) {
                    return this.tab === checkTab;
                };
            },
            controllerAs: "panels"
        }
    })
    .directive("wineReviews", function () {
        return {
            restrict: "E",
            templateUrl: "wine-reviews.html"
        };
    })
})();