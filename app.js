﻿/// <reference path="libs/angular.min.js" />

(function () {
    "use strict";

    angular.module("wineApp", ["wineComponents"])

    .controller("winecellarController", ["$http", function ($http) {
        var cellar = this;

        cellar.products = [];

        $http.get("/mock/vinliste.json").success(function (data) {
            cellar.products = data;
        });
    }])
    // *************************** Virker ikke... ***************************

    .controller("wineFormController", function () {
        this.newWine = {};

        this.addWine = function (product) {
            product.push(this.newWine);
            this.newWine = {};
        }
    })
    // *************************** Virker ikke... ***************************

    // ***************************Kode fra codeschool***************************
    .controller("ReviewController", function () {
        this.review = {};

        this.addReview = function (product) {
            this.review.createdOn = Date.now();
            product.reviews.push(this.review);
            this.review = {};
        };
    });
    // ***************************Kode fra codeschool***************************


})();