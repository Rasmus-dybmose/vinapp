Wine App

Program brugt: Visual Studio 2015
Tid brugt: 5 timer.
Inspiration: codeschool

Her kan man se en liste over vin (3 til at starte med).
Hver vin har et panel af tabs som man kan skifte i mellem alt efter hvad man vil se (Description, Details eller Reviews)

P� siderne beskrivelse og detaljer, kan man se noget ganske almindelig data som er hentet ned fra en json file ved hj�lp af http GET request.
P� Reviews siden kan man se nogle reviews og s� kan man ogs� selv oprette en hvis det er.

I bunden p� alle sider kan man se en knap der toggler en kasse hvor man skulle kunne oprette en vin hvis man ville. (Virker desv�rre ikke)�

Jeg har ikke rigtig gjort noget ud af stylingen da jeg f�rst og fremmest fors�gte at f� alt anden til at virke f�rst inden jeg begyndte p� at lave styling.  

Mvh.
Rasmus